window.React = require('react/addons');
window.Router = require('react-router');
window.Link = Router.Link;
window.moment = require('moment');

var $ = require('jQuery');
window.jQuery = $;

window.OptionalArray = React.PropTypes.array;
window.OptionalBool = React.PropTypes.bool;
window.OptionalFunction = React.PropTypes.func;
window.OptionalString = React.PropTypes.string;
window.OptionalNumber = React.PropTypes.number;
window.OptionalObject = React.PropTypes.object;

window.RequiredObject = React.PropTypes.object.isRequired;
window.RequiredArray = React.PropTypes.array.isRequired;
window.RequiredlBool = React.PropTypes.bool.isRequired;
window.RequiredFunction = React.PropTypes.func.isRequired;
window.RequiredString = React.PropTypes.string.isRequired;
window.RequiredNumber = React.PropTypes.number.isRequired;

window.Project = require('../../../common/project');
window.Plugins = require('../mixins/plugins');
window.Dispatcher = require('../../../common/dispatchers/dispatcher');
window.AppActions = require('../../../common/actions/app-actions');
window.Actions = require('../../../common/actions/action-constants');
window.Format = require('../../../common/utils/format');
window.Constants = require('./../../../common/constants');
window.Component = require('./../../../common/Component');

window.Input = require('../components/widget/Input');
window.Loader = require('../components/widget/Loader');
window.Modal = require('../components/widget/Modal');
window.Confirm = require('../components/widget/Modal');
window.UpDownValueClass = require('../components/widget/UpDownValueClass');
window.ErrorModal = require('../components/widget/ErrorModal');


window.AjaxHandler = require('./ajax-handler');
window.Utils = require('../../../common/utils/utils');
window.Auth = require('./auth');

window.Reactable = require('../lib/reactable');
window.Table = Reactable.Table;
window.Tr = Reactable.Tr;
window.Td = Reactable.Td;



window.log = function () {
    if (Project.debug) {
        console.log(arguments);
    }
};