/**
 * Created by kyle-ssg on 02/03/15.
 */
module.exports = {
    error: function (store, res) {

        var error = res.statusText || res.message,
            url = res.url;

        try {
            error = JSON.parse(res._bodyText).error.message;
        } catch (e) {
        }


        switch (res.status) {
            case 404:
                ErrorModal(null, error + ' Not found: ' + url);
                break;
            case 503:
                ErrorModal(null, 'Service unavailable: ' + url);
                break;
            default:
                ErrorModal(null, error);

        }


        if (store) {
            store.error = error;
            store.goneABitWest();
        }
    }
};