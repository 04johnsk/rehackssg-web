var Auth = {
    Google: {},
    Facebook: {}
};

Auth.Google.login = function (callback) {
    gapi.client.setApiKey(Project.google.APIKey);
    gapi.auth.authorize({
        client_id: Project.google.clientID,
        scope: 'email profile',
        prompt: 'select_account'
    }, function (r) {
        if (callback) {
            callback(r.access_token);
        }
    });
};


Auth.Facebook.login = function (callback) {
    FB.login(function(r){
        if (callback) {
            callback(FB.getAccessToken());
        }
    }, {scope: 'public_profile,email'});

};


    FB.init({
        appId      : Project.facebook.appId,
        xfbml      : true,
        version    : 'v2.3'
    });


//social login - all oauth handlers should call this
Auth._socialLogin = function (from, token, callback) {
    $.ajax({
        url: '/login=' + token,
        data: {
            from: from,
            token: token
        },
        type: 'get',
        dataType: "json",
        success: function (response) {
            if (callback) {
                callback(response);
            }
        }
    });
};

module.exports = Auth;