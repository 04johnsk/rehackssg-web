/** @jsx React.DOM */

var Global = require('./project/window.js');
var App = require('./components/App.js');
var AccountStore = require('../../common/stores/account-store');

var LandingPage = require('./components/pages/LandingPage.js');
var BasketPage = require('./components/pages/BasketPage');
var AccountLandingPage = require('./components/pages/AccountLandingPage.js');
var NotFoundPage = require('./components/pages/NotFoundPage.js');

var Router = require('react-router');
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var NotFoundRoute = Router.NotFoundRoute;
var RouteHandler = Router.RouteHandler;
var Transition = Router.Transition;

//Define our routes
var routes = (
    <Route handler={App} path="/">
        <DefaultRoute handler={AccountLandingPage} />
        <NotFoundRoute handler={NotFoundPage} />
        <Route name="app" handler={AccountLandingPage}></Route>
        <Route name="login" handler={AccountLandingPage}></Route>
        <Route name="basket" handler={BasketPage}></Route>
    </Route>
);

window.history.replaceState(null, null, Router.HistoryLocation.getCurrentPath().replace(/app\?path=\//, ''));

Router.run(routes, Router.HistoryLocation, function (Handler, options) {

    var isPublic = options.routes[1] ? options.routes[1].name == 'resetpassword' : false;

    React.render(<Handler isPublic={isPublic}/>, document.getElementById('main'));
});