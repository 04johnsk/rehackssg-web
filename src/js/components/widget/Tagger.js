/** @jsx React.DOM */
var ITagger = require('../../../common/interfaces/ITagger');

module.exports = Component(_.merge({}, ITagger,
    {
        displayName: 'Tagger',
        focus: function() {
            var activeObj = document.activeElement;
            activeObj.blur();

            this.refs.input.getDOMNode().focus()
        },
        onKeyUp: function(e) {
            var input = this.refs.input.getDOMNode();
            if (Utils.backspacePressed(e) && !input.value) {
                this._backspacePressed();
            }
        },
        onBlur: function(e) {
            var input = this.refs.input.getDOMNode();
            if (input.value) {
                this.add(input.value);
                input.value = "";
                return e.nativeEvent.preventDefault();
            }
        },
        onKeyDown: function(e) {
            var input = this.refs.input.getDOMNode();
            if (Utils.tabPressed(e) && input.value) {
                this.add(input.value);
                input.value = "";
                return e.nativeEvent.preventDefault();
            }
        },
        onKeyPress: function(e) {
            var input;
            if (Utils.enterPressed(e) || this.isDelimiter(String.fromCharCode(e.which))) {
                input = this.refs.input.getDOMNode();
                this.add(input.value);
                input.value = "";
                return e.nativeEvent.preventDefault()
            }
        },
        componentDidUpdate: function() {
          if (this.props.focus) {
              this.focus()
          }
        },
        render: function () {
            var items = this.state.items;
            return (
                <div className="tagger-container">
                    <label>Enter your friends email addresses</label>
                    <ul onClick= {this.focus} className="tagger textarea">
                        {
                            items.map(function (result, index) {
                                return (
                                    <li className={"tagger-new "  + (this.isValid(result) ? '' : 'invalid')} >
                                        {result}
                                        <span onClick={_.partial(this.remove, index)} className='text-icon'>×</span>
                                    </li>
                                )
                            }.bind(this))
                        }
                        <li class="tagger-new">
                            <input placeholder={this.props.placeholder} onBlur={this.onBlur} onKeyDown={this.onKeyDown} onKeyPress={this.onKeyPress}  onKeyUp={this.onKeyUp} type="text" ref="input"/>
                            <span role="status" aria-live="polite" class="ui-helper-hidden-accessible"></span>
                        </li>
                    </ul>
                </div>
            )
        }

    }
));