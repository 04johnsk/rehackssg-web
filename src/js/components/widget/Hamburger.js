/** @jsx React.DOM */
var ISingleSelect = require('../../../../common/interfaces/select/ISingleSelect');

module.exports = Component(_.merge({}, ISingleSelect,
    {
        displayName: 'Hamburger',
        render: function () {
            return (
                <span className={"hamburger " + (this.isSelected('in') ? 'in' : 'out')}>
                    <span className="hamburger-btn" onClick={_.partial(this.toggle, this.isSelected('in') ? 'out' : 'in')}>
                        <span className="fa fa-bars"></span>
                    </span>
                    <div className='hamburger-menu'>
                        { this.props.children[1] }
                    </div>
                </span>
            )
        }

    }
));