//Creates a toggle based div select box
module.exports = Component({
    getInitialState: function () {
        return {
            open: false,
            display: this.props.display
        }
    },
    toggle: function () {
        this.setState({
            open: !this.state.open
        })
    },
    componentDidMount: function () {
        _.each(this.refs.children.getDOMNode().childNodes, function (node) {
            //on click update the select title
            node.onclick = function (e) {
                var value = e.currentTarget.getAttribute('value'),
                    display = e.currentTarget.getAttribute('data-display');
                if (this.state.value != value) {
                    this.props.onChange(value)
                }
                this.setState({
                    display: display,
                    value: value
                });
            }.bind(this)
        }.bind(this))

    },
    render: function () {
        var className = this.props.className || '';
        return (
            <div onClick={this.toggle} className={className + (this.state.open ? ' select open' : ' select') }>
                <div className="select-container">
                    <div className="current-selection">
                        { this.props.title }
                        <div className="arrow-select"></div>
                    </div>
                    <div ref="children" className="children">
                    { this.props.children }
                    </div>
                </div>
            </div>
        )
    }
})