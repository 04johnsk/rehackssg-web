/** @jsx React.DOM */
var IValueTracker = require('../../../../common/interfaces/widget/IValueTracker');
module.exports = Component(_.merge({}, IValueTracker, {
    displayName: 'ValueTracker',

    propTypes: _.merge({}, {
        className: OptionalString
    }, IValueTracker.propTypes),

    render: function () {
        var className = this.props.className || '';
        if (this.state.change > 0) {
            className += ' up';
        } else if (this.state.change < 0) {
            className += ' down'
        }
        return (
            <span className={className}>
                { this.props.children || this.props.value }
            </span>

        );
    }
}));