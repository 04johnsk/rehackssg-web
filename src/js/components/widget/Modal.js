module.exports = {

    isVisible: false,
    className: '',

    transitionTo: function(path, params) {
      this.props.context.transitionTo(path, params);
    },

    componentDidMount: function () {
        $(this.getDOMNode()).on('hidden.bs.modal', this._closed);
        $(this.getDOMNode()).on('shown.bs.modal', this._shown);
        $(this.getDOMNode()).modal({background: true, keyboard: true, show: true});
    },

    hide: function() {
        $(this.getDOMNode()).off('hidden.bs.modal', this.unmount);
        $(this.getDOMNode()).off('shown.bs.modal', this._shown);
        $(this.getDOMNode()).modal('hide');
    },

    show: function() {
        $(this.getDOMNode()).on('hidden.bs.modal', this.unmount);
        $(this.getDOMNode()).modal('show');
    },

    close: function() {
        $(this.getDOMNode()).modal('hide')
    },

    _shown: function() {
        _.delay(this.shown, 1, 'later')
        this.isVisible = true;

    },

    shown: function() {

    },

    closed: function() {

    },

    _closed: function() {
        this.closed()
        React.unmountComponentAtNode(document.getElementById('modal'));
    },

    header: function () {
        return this.props.header || '';
    },

    body: function () {
        return this.props.body || '';
    },

    footer: function () {
        return this.props.body || '';
    },

    render: function () {
        return (
                <div tabIndex="-1" className={"modal fade " + (this.isVisible ? 'in ' :'') + this.className}  role="dialog" aria-hidden="true">
                    <div className="modal-wrapper">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <i className="icons-close-white modal-close" onClick={this.close}></i>
                                <div className="modal-header">{ this.header()  }</div>
                                <div className="modal-body">
                                    { this.body()  }
                                </div>
                                <div className="modal-footer">{ this.footer()  }</div>
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
};
