var Modal = require('./Modal.js');
var View = Component(_.merge({}, Modal, {
    displayName: 'Error',

    header: function () {
        return (
            <div>
                There seems to be a problem
            </div>
        )
    },

    footer: function () {
        return (
            <div>
                <button type="button" className="btn dark short" onClick={this.close}>Ok</button>
            </div>
        )
    },

    render: function () {
        return (
            <div tabIndex="-1" className="modal alert fade" role="dialog" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">{ this.header() }</div>
                        <div className="modal-body">{ this.body() }</div>
                        <div className="modal-footer">{ this.footer() }</div>
                    </div>
                </div>
            </div>
        );
    }

}));

module.exports =
    function(header, body) {
        React.render(<View header={header} body={body} />, document.getElementById('modal'));
    }
;