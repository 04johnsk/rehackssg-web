var Modal = require('./Modal.js');
var View = Component(_.merge({}, Modal, {
    displayName: 'Confirm',

    onNo: function() {
        if (this.props.onNo) {
            this.props.onNo();
        }
        this.close();
    },

    onYes: function() {
        this.props.onYes();
        this.close();
    },

    closed: function() {
        this.onNo();
    },

    footer: function () {
        return (
            <div>
                <button type="button" className="btn dark short" onClick={this.onNo}>{this.props.noText || 'No'}</button>
                <button type="button" className="btn primary short" onClick={this.onYes}>{this.props.yesText || 'Yes'}</button>
            </div>
        )
    },

    render: function () {
        return (
            <div tabIndex="-1" className="modal alert fade" role="dialog" aria-hidden="true">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">{ this.header()  }</div>
                        <div className="modal-body">{ this.body()  }</div>
                        <div className="modal-footer">{ this.footer()  }</div>
                    </div>
                </div>
            </div>
        );
    }

}));

module.exports =
    function(header, body, onYes, onNo) {
        React.render(
            <View header={header} body={body}
                onYes={onYes} onNo={onNo} />, document.getElementById('modal'));
    }
;