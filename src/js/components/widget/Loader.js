/**
 * Created by kylejohnson on 08/04/15.
 */
module.exports = Component({
    displayName: 'Loader',
    render: function () {
        return (
            <div className="loading">
                <img src="/images/loader.svg" alt="Page Loading" />
                <div>{this.props && this.props.text || ''}</div>
            </div>
        )
    }
});