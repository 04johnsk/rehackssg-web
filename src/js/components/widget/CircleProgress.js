/**
 * Created by kylejohnson on 25/06/15.
 */
module.exports = Component({
    render: function () {
        return (
            <div className="c100 p25 big center">
                <span>{this.props.percent}%</span>

                <div className="slice">
                    <div className="bar"></div>
                    <div className="fill"></div>
                </div>
            </div>
        )
    }
})