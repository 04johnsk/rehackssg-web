/**
 * Created by kylejohnson on 21/05/15.
 */
/**
 * Created by kylejohnson on 18/05/15.
 */
module.exports = Component({
    getInitialState: function () {
        return {
            blurred: false
        }
    },
    componentDidMount: function () {
        _.bindAll(this, 'onBlur');
        this.getDOMNode().onblur = this.onBlur;
    },

    onBlur: function () {
        this.setState({
            blurred: true
        })
    },

    render: function () {
        var validation = '';

        if (this.state.blurred) {
            validation = this.props.valid ? 'valid' : 'invalid'
        }

        return (

            <div className="form-group">
                <div className={this.props.className + (this.state.blurred ? ' input-group ' : '') + validation}>
                    <input className="form-control" onBlur={this.onBlur} type={this.props.type || "text"} placeholder={this.props.placeholder} name={this.props.name}  onChange={this.props.onChange} value={this.props.value}/>
                        {this.state.blurred ? <span className="input-group-addon">
                    <i className={"fa " + (this.props.valid ? 'fa-check' : 'fa-warning')}/>
                </span> : null }
                </div>
            </div>

        )
    }
});