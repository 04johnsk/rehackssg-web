/**
 * Created by kylejohnson on 17/05/15.
 */
var ButtonsStore = require('../../../../common/stores/buttons-store'),
    ButtonStore = require('../../../../common/stores/button-store');

module.exports = Component({
    getButtonsState: function () {
        return {
            buttons: ButtonsStore.getButtons(),
            ready: ButtonsStore.hasLoaded
        }
    },
    getButtonState: function () {
        return {
            processing: ButtonStore.getLoading()
        }
    },
    onButtonChanged: function () {
        this.setState(this.getButtonState())
    },
    getInitialState: function () {
        return _.merge({}, this.getButtonState(), this.getButtonsState());
    },
    componentWillMount: function () {
        AppActions.getButtons();
        this.listenTo(ButtonsStore, 'change', this.onButtonsLoaded);
        this.listenTo(ButtonStore, 'change', this.onButtonChanged);
    },
    onButtonsLoaded: function () {
        this.setState(this.getButtonsState());
    },
    render: function () {
        return this.state.ready ? (
            <div className="panel">
                <div className="panel-head">
                    <h4>Your buttons</h4>
                </div>
                <div className="panel-body">
                 {
                     this.state.buttons ? (
                         <table className="table">
                             <tbody>
                                {
                                    _.map(this.state.buttons, function (button) {
                                        var isLoading = this.state.processing[button];
                                        return (
                                            <tr>
                                                <td>
                                                    <label>{button}</label>
                                                </td>
                                                <td>
                                                    <button disabled={isLoading} onClick={_.partial(AppActions.clickButton, button)} className="btn btn-grey">
                                                        {isLoading ? 'Loading' : 'Click me!'}
                                                    </button>
                                                </td>
                                            </tr>
                                        )
                                    }.bind(this))
                                }
                             </tbody>
                         </table>) : 'You have no buttons paired'
                     }
                </div>
            </div>
        ) : <Loader/>;
    }
});