/** @jsx React.DOM */

var IApp = require('../../../common/interfaces/IApp'),
    RouteHandler = require('react-router').RouteHandler,
    Cookies = require('cookies-js'),
    HeaderLoggedOut = require('./header/HeaderLoggedOut'),
    HeaderLoggedIn = require('./header/HeaderLoggedIn'),
    LandingPage = require('./pages/LandingPage');

module.exports = Component(_.merge({}, IApp,
    {
        displayName: 'App',

        mixins: [Router.Navigation],

        checkLoginToken: function () {
            var type = Cookies.get('loginType'),
                token = Cookies.get('token');
            if (token) {
                this.loginWithToken(type, token);
            } else {
                this.setState({ready: true});
            }
        },

        _onLogin: function () {

        },

        _onLogout: function () {
            Cookies.expire('token');
            Cookies.expire('loginType');
            this.replaceWith('app')
        },

        createLoginToken: function (token, loginType) {
            Cookies.set('token', token);
            Cookies.set('loginType', loginType);
        },

        render: function () {
            var loggedIn = this.state.account;
            return (
                <div>
                    { loggedIn ? <HeaderLoggedIn /> : <HeaderLoggedOut onLogout={this._onLogout} I={!this.state.ready} /> }
                    { loggedIn || this.props.isPublic ? <RouteHandler/> : <LandingPage loading={!this.state.ready}/> }
                    <div ref='modal'></div>
                </div>
            );

        }
    }));