ForgotPasswordStore = require('../../../../common/stores/forgot-password-store');
module.exports = Component(_.merge({}, Modal, {
    displayName: 'ForgotPasswordModal',
    className: 'forgot-password-modal',

    getInitialState: function () {
        return {
            emailAddress: ''
        }
    },
    componentWillMount: function() {
        this.listenTo(ForgotPasswordStore, 'change', this._storeChanged);
        this.listenTo(ForgotPasswordStore, 'saved', this._storeSaved);

    },
    _storeChanged: function() {
      this.setState({
          saving: ForgotPasswordStore.isSaving
      });
    },
    _detailsChange: function (e) {
        this.setState({
            emailAddress: Utils.safeParseEventValue(e)
        });
    },
    _storeSaved: function() {
      this.setState({
          saved: true
      })
    },
    process: function () {
        AppActions.forgotPassword(this.state.emailAddress);
    },
    header: function () {
        return <h4>Forgot password</h4>
    },
    body: function () {
        return this.state.saved? (
                <div>Your request has been processed, please check your email and follow the steps provided.</div>
            ): (
            <div className="forgot-password-form">
                <h4>Please enter your email address</h4>
                <fieldset className="form-group">
                    <input type="text" placeholder="example@example.com" name="emailAddress"  onChange={_.partial(this._detailsChange)} className={ this.req(Utils.validate.email(this.state.emailAddress))}/>
                </fieldset>
            </div>
        )
    },
    footer: function () {
        return this.state.saved? (
            <div className="col-md-12 form-actions">
                <button type="button" className="btn reject short" onClick={this.close}>Continue</button>
            </div>
        ) :(
            <div className="col-md-12 form-actions">
                <button type="button" className="btn reject short" onClick={this.close}>Cancel</button>
                <button type="button" onClick={this.process} disabled={!Utils.validate.email(this.state.emailAddress) || this.state.saving} className="btn position short">{this.state.saving ? 'Processing' : 'Process' }</button>
            </div>
        )
    }
}));