/** @jsx React.DOM */
var ILoginItem = require('../../../../common/interfaces/ILoginItem'),
    ForgottenPasswordModal = require('../modals/ForgotPasswordModal');


module.exports = Component(_.merge({}, ILoginItem, {
    displayName: 'HeaderLoggedOut',


    forgottenPassword: function () {
        React.render(<ForgottenPasswordModal />, document.getElementById('modal'));
    },

    render: function () {
        var loginDetails = this.state.details;
        return (
            <header id="top" role="banner">
                <nav className="navbar navbar-default">
                    <div className="container-fluid">

                        {this.state.loggingIn || !this.state.ready ? (
                            <div className="text-center">
                                <h4 className="" to="app">{Constants.TITLE}</h4>
                            </div>
                        ) : (
                            <div>
                                <div className="text-center">
                                    <h4 className="" to="app">{Constants.TITLE}</h4>
                                </div>
                                <div className="navbar-form navbar-right" role="search">
                                    <div className="form-group">
                                        <div className="input-group">
                                            <input type="email" placeholder="your@mail.com" name="email"
                                                   onChange={_.partial(this._loginDetailsChange, 'emailAddress')}
                                                   className={ 'form-control tall ' + this.req(Utils.validate.email(loginDetails.emailAddress)) }
                                                   value={loginDetails.emailAddress}/>
                                        </div>
                                        <div className="input-group">
                                            <input className="input" type="password" placeholder="Password"
                                                   name="password"
                                                   onChange={_.partial(this._loginDetailsChange, 'password')}
                                                   className={ 'form-control tall ' + this.req(loginDetails.password) }
                                                   value={loginDetails.password}/>
                                        </div>
                                        <button disabled={!this.state.loginDetailsValid} onClick={this.loginWithEmail}
                                                className="btn primary short">{this.state.loggingIn ? 'Logging in' : 'Sign In'}</button>
                                    </div>
                                    <div className="form-group">
                                    </div>
                                    <span onClick={this.forgottenPassword}
                                          className="link text-center">Forgot{'?'}</span>
                                </div>
                            </div>
                        )}
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-2">

                        </div>
                    </div>
                </nav>
            </header>
        )
    }
}));