/** @jsx React.DOM */
var INavigationItem = require('../../../../common/interfaces/INavigationItem'),
    Hamburger = require('../widget/Hamburger'),
    ButtonList = require('../buttons/ButtonList')
module.exports = Component(_.merge({}, INavigationItem, {
    displayName: 'HeaderLoggedIn',

    mixins: [Router.Navigation],

    onLogoutClick: function () {
        this.replaceWith('app');
        this.logout();
    },

    render: function () {
        return (
            <header id="top" role="banner">
                <nav className="navbar navbar-default">
                    <div className="text-center">
                        <h4 className="" to="app">{Constants.TITLE}</h4>
                    </div>
                </nav>
            </header>
        );
    }
}));