/**
 * Created by kylejohnson on 25/05/15.
 */
module.exports = Component(_.merge({}, {

    mixins: [React.addons.PureRenderMixin],
    productActions: function () {

    },
    render: function () {
        var product = this.props.product,
            quantity = this.props.quantity,
            price = parseInt(quantity) * product.price;
        return (
            <div className="basket">
                <img align="left" src={Constants.productImages + product.image} />
                <UpDownValueClass className="price" cooldown={500} value={ price }>&pound;{Format.money(product.price)} x {quantity} = &pound;{Format.money(price)}</UpDownValueClass>
                <div className="product-name">{product.name}</div>
            </div>
        )
    }


}));