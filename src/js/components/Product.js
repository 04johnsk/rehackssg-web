/**
 * Created by kylejohnson on 25/06/15.
 */
module.exports = Component({
    render: function () {
        var product = this.props.product,
            percentageRemaining = (product.remaining / product.capacity) * 100,
            className = '';
        if (percentageRemaining < 50) {
            className = 'orange';
        }
        if (percentageRemaining < 20) {
            className = 'red';
        }
        return (
            <li className={"list-item " + (product.status ? ' item-active' : '')}>
                <div className="status-container">
                    <span className={"fa status fa-circle" + (product.status ? ' active' : '')}/>
                </div>
                <div className="image-container">
                    <img className="pull-left"
                         src={product.image}
                         height="80px"/>
                </div>
                <div>
                    <div>
                        <h4>
                            {product.name} {product.loading ?
                            <span className="product-loading">
                                 <img className="loader-inline" src="/images/loader.svg" alt="Page Loading"/></span> : product.ordered ?
                            <span className="price"><span
                                className="fa fa-cart-arrow-down added-to-basket"/>£{Format.money(product.price)}</span> : null}
                        </h4>
                        <h5>
                            {product.remaining}{product.units}/{product.capacity}{product.units} <span
                            className={"last-updated"}>{moment(product.date).fromNow()}</span>

                        </h5>
                    </div>

                    <div className="meter">
                        <span className={className} style={{"width": percentageRemaining + '%'}}></span>
                        {!product.loading && product.ordered? <span className="due-date">Due in 2 days</span> : ''}
                    </div>
                </div>
            </li>
        )
    }
})