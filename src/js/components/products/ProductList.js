/**
 * Created by kylejohnson on 25/05/15.
 */
var IProductList = require('../../../../common/interfaces/IProductList'),
    ProductFilter = require('./ProductFilter'),
    ProductSummary = require('./ProductSummary');
module.exports = Component(_.merge({}, IProductList, {

    displayName: 'ProductList',
    render: function () {
        var pairings = this.state.pairings || [];

        return this.state.productsReady ? (
            <div className="container-fluid">
                <div className="col-md-12">
                    <ProductFilter onChange={this.filter} />
                </div>
            {
                this.state.products ?
                    _.map(this.state.products, function (product) {
                        return (
                            <div key={product.id} className="col-md-3">
                                <ProductSummary isPaired={pairings[parseInt(product.id)]} product={product}/>
                            </div>
                        )
                    }.bind(this)) : 'No products found'
                }

            </div>
        ) : <Loader/>
    }


}));