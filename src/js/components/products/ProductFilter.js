module.exports = Component({
    render: function () {
        return (
            <div className="form-group">
                <div className="input-group">
                    <div className="input-group-addon">
                        <span className="fa fa-search"/>
                    </div>
                    <input className="form-control" type="text" onChange={this.props.onChange}/>
                </div>
            </div>
        )
    }
})