/**
 * Created by kylejohnson on 25/05/15.
 */
module.exports = Component(_.merge({}, {

    mixins: [React.addons.PureRenderMixin],
    productActions: function() {

    },
    render: function () {
        var product = this.props.product;
        return (
            <div className="product">
                <div onClick={this.productActions} className="panel-addon product-actions">
                    <span className="fa fa-ellipsis-h"/>
                </div>
                <div className="product-image">
                    <img src={Constants.productImages + product.image} />
                    <button className="btn btn-success">&pound;{Format.money(product.price)} {this.props.isPaired ? (
                        <span className="fa fa-plug"></span>
                    ): null}</button>
                </div>
                <div className="product-details">
                    <div className="product-name">{product.name}</div>
                </div>
            </div>
        )
    }


}));