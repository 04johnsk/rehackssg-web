/**
 * Created by kyle-ssg on 12/02/15.
 */
/** @jsx React.DOM */
var Highcharts = require('react-highcharts/more');

var ProductList = require('../products/ProductList'),
    ProductStore = require('../../../../common/stores/product-store'),
    Product = require('../Product');
module.exports = Component(_.merge({
    displayName: 'AccountLandingPage',

    getInitialState: function () {


        var gaugeOptions = {

            chart: {
                type: 'gauge'
            },

            title: null,

            pane: {
                center: ['50%', '85%'],
                size: '140%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },

            tooltip: {
                enabled: false
            },

            // the value axis
            yAxis: {
                min: 0,
                max: 300,
                title: {
                    text: 'Weight (ml)'
                },
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                lineWidth: 0,
                minorTickInterval: null,
                tickPixelInterval: 50,
                tickWidth: 0,
                title: {
                    y: -70
                },
                labels: {
                    y: 16
                }
            },

            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        borderWidth: 0,
                        useHTML: true
                    }
                }
            }
        };
        return {
            step: 1,
            config: _.merge({}, gaugeOptions,
                {

                    credits: {
                        enabled: false
                    },

                    series: [{
                        name: 'Speed',
                        data: [80],
                        dataLabels: {
                            format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                            '<span style="font-size:12px;color:silver">ml</span></div>'
                        },
                        tooltip: {
                            valueSuffix: ' km/h'
                        }
                    }]
                }
            ),
            demoProduct:{
                name: 'Cif Bathroom Moose',
                image: "http://www.unilever.com.ph/Images/cif-mousse-450x450_tcm103-290592.jpg",
                status: 0,
                capacity: 500,
                remaining: 200,
                price: 350,
                units: 'ml',
                date: moment().add('minutes', -590).toISOString()
            },
            mockProducts: [
                {
                    name: 'Cif Bathroom Spray',
                    image: "http://www.unilever.com.ph/Images/cif-bathroom-450x450_tcm103-290583.jpg",
                    status: 1,
                    capacity: 700,
                    remaining: 10,
                    price: 250,
                    ordered: true,
                    units: 'ml',
                    date: moment().add('minutes', -160).toISOString()
                },
                {
                    name: 'Surf Relaxing Lavendar',
                    image: "http://www.unilever.com.ph/Images/Surf%20Powder%20Relaxing%20Lavender%20450x450_tcm103-290807.jpg",
                    status: 1,
                    capacity: 1.6,
                    remaining: 0.9,
                    units: 'kg',
                    date: moment().add('minutes', -20).toISOString()
                }
            ]
        }
    },
    componentDidMount: function() {
        this.listenTo(ProductStore, 'change', this.productAdded);
        this.listenTo(ProductStore, 'nfc', this.triggerStep);

        $(document).on('keyup', function(e){
            setTimeout(function(){
                this.setStep(this.state.step +1);
            }.bind(this), 500);
        }.bind(this))

    },
    triggerStep: function() {
      this.setStep(this.state.step+1);
    },
    setStep: function(num) {
        var product = this.state.demoProduct;
        if (num == 2) {
            product = _.merge(product, { // product put in scales
                date: moment(),
                status: 1,
            });
            this.setState({
                demoProduct: product,
                step: num
            });
        } else if (num == 3) { //product taken out of scales
            product = _.merge(product, {
                date: moment(),
                status: 0
            });

            this.setState({
                demoProduct: product,
                step: num
            });
        } else  if (num == 4) {
            AppActions.addProductToBasket();
            product = _.merge(product, { // product used and put back in scales
                remaining: 50,
                date: moment(),
                ordered: true,
                loading: true,
                status: 1
            });

            this.setState({
                demoProduct: product,
                step: num
            });
        }
    },
    productAdded: function() {
      this.setState({
          demoProduct: _.merge(this.state.demoProduct, {loading: false})
      })
    },
    render: function () {
        return (
            <div className="container-fluid">
                <div className="row">
                    <ul className="list-unstyled">
                        <Product demo={true} product={this.state.demoProduct}/>
                    </ul>
                    <ul className="list-unstyled">
                        {
                            _.map(this.state.mockProducts, function (mockProduct) {
                                return <Product product={mockProduct}/>
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}));
