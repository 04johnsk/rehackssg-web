/**
 * Created by kyle-ssg on 12/02/15.
 */
/** @jsx React.DOM */

module.exports = Component({
    displayName: 'NotFoundPage',

    render: function () {
        return (
            <div className="container">
                Page not found
            </div>
        );
    }
});