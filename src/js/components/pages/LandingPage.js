/**
 * Created by kyle-ssg on 12/02/15.
 */
/** @jsx React.DOM */
var ILoginItem = require('../../../../common/interfaces/ILoginItem');
module.exports = Component(_.merge(ILoginItem, {
    displayName: 'LandingPage',

    createGoogleLoginButton: function () {
        return {
            __html: '<button class="btn google full-width" onclick="login(' + this.state.rememberMe + ')" >Google+</button>'
        }
    },
    createFacebookLoginButton: function () {
        return {
            __html: '<button class="btn facebook full-width" onclick="facebookLogin(' + this.state.rememberMe + ')" >Facebook</button>'
        }
    },

    loading: function () {
        return (
            <div className="container-fluid">
                <Loader text={this.props.loading ? 'Loading Config' : 'Logging In'}/>
            </div>
        )
    },
    render: function () {
        var details = this.state.details,
            loginDetails = this.state.loginDetails;
        return (

            this.state.loggingIn || this.props.loading ? this.loading()
                : (
                <div className="container-fluid">

                    <div className="login-container clearfix">

                        <div className="pull-left col-md-12">
                            <h2 className="">Continue With</h2>

                            <div className="col-md-12 input-group">
                                    { this.state.loggingIn ? <span>Logging in</span> : <div dangerouslySetInnerHTML={this.createGoogleLoginButton()}></div> }
                            </div>
                            <br/>
                            <div className="col-md-12 input-group">
                                    { this.state.loggingIn ? <span>Logging in</span> : <div dangerouslySetInnerHTML={this.createFacebookLoginButton()}></div>}
                            </div>
                        </div>

                        <div className="col-md-12 form">
                            <h2 className="">Or sign up</h2>
                            <Input onChange={_.partial(this._detailsChange, 'firstName')} placeholder="First name" name="firstName" valid={details.firstName}/>
                            <Input onChange={_.partial(this._detailsChange, 'lastName')} placeholder="Last name" name="lastName" valid={details.lastName}/>
                            <Input type="email" onChange={_.partial(this._detailsChange, 'emailAddress1')} placeholder="Email address" name="email" valid={Utils.validate.email(details.emailAddress1)}/>
                            <Input type="email" onChange={_.partial(this._detailsChange, 'emailAddress2')} placeholder="Confirm email address" name="email" valid={details.emailAddress1 == details.emailAddress2}/>
                            <Input type="password" onChange={_.partial(this._detailsChange, 'password1')} placeholder="Password" name="password" valid={details.password1}/>
                            <Input type="password" onChange={_.partial(this._detailsChange, 'password2')} placeholder="Confirm password" name="password2" valid={details.password1 == details.password2}/>
                            <div className="form-actions">
                                <button disabled={!this.state.detailsValid} onClick={this.register} className="btn primary short pull-right">Sign Up</button>
                            </div>
                        </div>

                    </div>
                </div>


            )

        );
    }
}));
