/**
 * Created by kyle-ssg on 12/02/15.
 */
/** @jsx React.DOM */
var IBasket = require('../../../../common/interfaces/IBasket'),
    CircleProgress = require('../widget/CircleProgress'),
    BasketItem = require('../BasketItem');
module.exports = Component(_.merge({}, IBasket, {
    displayName: 'AccountLandingPage',

    render: function () {
        var totalPrice = this.state.basketReady ? _.sum(this.state.basket, function (quantity, productId) {
            var product = this.state.products[parseInt(productId)];
            return product ? quantity * product.price : 0;
        }.bind(this)) : 0;
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-12">
                        <h2>Your Basket -
                            <UpDownValueClass className="price" cooldown={500}
                                              value={ totalPrice }> &pound;{Format.money(totalPrice)}</UpDownValueClass>
                        </h2>
                        {this.state.basketReady ? this.state.basketEmpty ? 'Your basket is empty' :
                            _.map(this.state.basket, function (quantity, productId) {
                                return <BasketItem quantity={quantity} product={this.state.products[productId]}/>
                            }.bind(this)) : <Loader/>}
                    </div>
                </div>
                <CircleProgress percent={10}/>
            </div>
        );
    }
}));
