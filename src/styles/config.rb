# Require any additional compass plugins here.

# Set this to the root of your project when deployed:
images_dir = "../images"
sass_dir = "sass"

#
# NOTE:
# if you make changes to this file, stop polling compass changes first (ctrl + c)
# the you'll probably need to make a change in screen.scss or some other file (i.e. .bla{}) to force it to recompile
#

# You can select your preferred output style here (can be overridden via the command line)
#
# default: :compressed
# change to :nested for local development
#
output_style = :compressed

# To enable relative paths to assets via compass helper functions.
#
relative_assets = true

# To display debugging comments that display the original location of your selectors,
# default: false
# change to true for local development
#
line_comments = false