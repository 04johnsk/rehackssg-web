module.exports = {
    debug: true,
    pusher: {
        key: 'f6ad493fe1eafa545f21',
        cluster: 'eu'
    },


    uploadCare: '',
    api: {
        apiary: 'http://private-9765b-spark6.apiary-mock.com/',
        live: 'http://private-8758c-fluxreactboilerplate.apiary-mock.com/',
        channels: 'http://private-8758c-fluxreactboilerplate.apiary-mock.com/',
        spark: 'https://api.spark.io/v1/',
        products: 'http://private-66ae8-giftsapi.apiary-mock.com/'
    },
    google: {
        clientID: "630357652827-aecvvjoij3tu7grfee057ndjkf6743s1.apps.googleusercontent.com",
        APIKey: 'AIzaSyC5mX6rTnIQ086weON_cdYyJS8IgZxJQP4',
        domain: ''
    },
    facebook: {
        appId: '892249750833438'
    },
    files: {
        require: ["js/app/app"],
        css: "css/compiled/screen.css"
    }
};
