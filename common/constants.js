var Constants = {
    defaults: {
        userImage: 'https://placehold.it/100x100.png',
    },

    aspectRatios: {
        user: {
            width: '34px',
            height: '34px'
        },
        userBig: {
            width: '170px',
            height: '170px'
        }
    },

    productImages: 'images/products/',

    TITLE: 'Unilver'
};

module.exports = Constants;