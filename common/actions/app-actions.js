module.exports = {
    login: function (rememberMe, type, token) { //Login with an unused token
        Dispatcher.handleViewAction({
            actionType: Actions.LOGIN,
            token: token,
            rememberMe: rememberMe,
            type: type
        })
    },
    loginWithToken: function (rememberMe, type, token) { //Login with a predetermined token,
        Dispatcher.handleViewAction({
            actionType: Actions.LOGIN_WITH_TOKEN,
            token: token,
            rememberMe: rememberMe,
            type: type
        })
    },
    loginWithEmail: function (loginDetails) {//Login with a email and password {emailAddress,password}
        Dispatcher.handleViewAction({
            actionType: Actions.LOGIN_WITH_EMAIL,
            loginDetails: loginDetails
        })
    },
    logout: function () {
        Dispatcher.handleViewAction({ //Logout
            actionType: Actions.LOGOUT
        })
    },
    register: function (registration) { //register with an email and password {firstName, lastName, emailAddress1,2 + password1,2}
        Dispatcher.handleViewAction({
            actionType: Actions.REGISTER,
            registration: registration
        })
    },
    forgotPassword: function(emailAddress) { //process forgot password
        Dispatcher.handleViewAction({
            actionType: Actions.FORGOT_PASSWORD,
            emailAddress: emailAddress
        })
    },
    getButtons: function() { //get a user's buttons
        Dispatcher.handleViewAction({
            actionType: Actions.GET_BUTTONS
        })
    },
    getProducts: function() { //get products
        Dispatcher.handleViewAction({
            actionType: Actions.GET_PRODUCTS
        })
    },
    clickButton: function(button) { //get a user's buttons
        Dispatcher.handleViewAction({
            actionType: Actions.ClICK_BUTTON,
            id: button
        })
    },
    setProductQuantity: function(productId, quantity) { //get a user's buttons
        Dispatcher.handleViewAction({
            actionType: Actions.SET_QUANTITY,
            productId: productId,
            quantity: quantity
        })
    },
    getBasket: function() {
        Dispatcher.handleViewAction({
            actionType: Actions.GET_BASKET
        })
    },
    addProductToBasket: function() {
        Dispatcher.handleViewAction({
            actionType: Actions.ADD_PRODUCT
        })
    }

};