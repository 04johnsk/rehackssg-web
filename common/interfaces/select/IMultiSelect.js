var ConfigStore = require('../../stores/config-store');

module.exports = {

    componentWillMount:function() {
        this.currentValue = this.props.value;
    },

    componentWillUpdate: function(newProps) {
        var array;
      if (newProps.value && this.props.value && newProps.value.length != this.props.value.length) {
          array = newProps.value;
          if (this.allEqualsNone && this.allValues && this.allValues.length == array.length) {
              array = [];
          }

          this.currentValue = array;

          this.forceUpdate();
      }
    },

    allSelected: function (allValues, allowIfNoneSelected) {
        if (allowIfNoneSelected && (!this.currentValue || !this.currentValue.length)) {
            return true;
        }
        return allValues.length == this.currentValue.length;
    },

    isSelected: function (value) {
        return this.currentValue ? this.currentValue.indexOf(value) !== -1 : false;
    },

    selectAll: function () {
        if (this.currentValue && this.currentValue.length) {
            this.currentValue = []
        } else if (this.allEqualsNone) {
            this.currentValue = []
        }
        else {
            this.currentValue = this.allValues;
        }
        this.forceUpdate();
    },

    toggle: function (e) {
        var value = e.currentTarget.value || e.currentTarget.getAttribute('value'),
            array = this.currentValue || [],
            index = array.indexOf(value);

        e.currentTarget.blur(); // not sure why, but it seems so

        if (index === -1) {
            array.push(value);
        } else {
            array.splice(index, 1);
        }

        if (this.allEqualsNone && this.allValues && this.allValues.length == array.length) {
            array = [];
        }

        this.currentValue = array;


        if (this.props.onChange) {
            this.props.onChange(array);
        }
        this.forceUpdate();

    }
};
/**
 * Created by kyle-ssg on 14/02/15.
 */
