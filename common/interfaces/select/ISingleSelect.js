var ConfigStore = require('../../stores/config-store');

module.exports = {

    getInitialProps: function () {
        return {
            value: this.props.value || null
        }
    },

    isSelected: function(value) {
        return (this.props.value) == value;
    },

    toggle: function(e) {

        var value = Utils.safeParseEventValue(e);

        if (e.currentTarget) {
            e.currentTarget.blur(); // not sure why, but it seems so
        }


        if (value != this.props.value) {
            this.props.value = value;
            if (this.props.onChange) {
                this.props.onChange(value);
            }
            this.forceUpdate();
        }


    }
};
/**
 * Created by kyle-ssg on 14/02/15.
 */
