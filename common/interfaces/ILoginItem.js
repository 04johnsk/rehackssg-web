var AppActions = require('../actions/app-actions'),
    AccountStore = require('../stores/account-store');


window.login = function(rememberMe) {
    Auth.Google.login(function(token) {
        AppActions.login(rememberMe, 'GOOGLE', token)
    })
};

window.facebookLogin = function(rememberMe) {
    Auth.Facebook.login(function(token) {
        AppActions.login(rememberMe, 'FACEBOOK', token)
    })
};


module.exports = {
    getInitialState: function() {
        return {
            rememberMe: true,
            details: {},
            resetDetails: {},
            loginDetails: {},
            detailsValid: false,
            resetDetailsValid: false
        }
    },
    _detailsChange: function (path, e) {
        var details = this.state.details;
        details[path] = Utils.safeParseEventValue(e);
        this.setState({
            details: details,
            detailsValid: AccountStore.registrationDetailsValid(details)
        });
    },
    _resetDetailsChange: function (path, e) {
        var details = this.state.resetDetails;
        details[path] = Utils.safeParseEventValue(e);
        this.setState({
            resetDetails: details,
            resetDetailsValid: AccountStore.resetDetailsValid(details)
        });
    },
    _loginDetailsChange: function (path, e) {
        var details = this.state.details;
        details[path] = Utils.safeParseEventValue(e);
        this.setState({
            loginDetails: details,
            loginDetailsValid: AccountStore.loginDetailsValid(details)
        });
    },
    componentWillMount:function() {
        this.listenTo(AccountStore,'change',this._accountChanged);
    },
    _accountChanged:function() {
        if (this._lifeCycleState != "UNMOUNTED") {
            this.setState({
                loggingIn: AccountStore.isLoading
            });
        }
    },
    _rememberMeChanged: function(e) {
        this.setState({
            rememberMe: !this.state.rememberMe
        });
    },
    register: function () {
        AppActions.register(this.state.details);
    },
    loginWithEmail: function () {
        AppActions.loginWithEmail(this.state.loginDetails);
    },
    resetPassword: function(code) {
        this.state.resetDetails.code = code;
        AppActions.resetPassword(this.state.resetDetails);
    },
    logout: function () {
        AppActions.logout();
    }
};

