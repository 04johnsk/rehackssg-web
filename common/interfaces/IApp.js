var ConfigStore = require('../stores/config-store'),
    AccountStore = require('../stores/account-store');
module.exports = {

    getInitialState: function () {
        return {
            account: AccountStore.model,
            isLoading: AccountStore.isLoading,
            ready: ConfigStore.hasLoaded,
            config: ConfigStore.model
        }
    },
    componentWillMount: function () {
        this.listenTo(AccountStore, 'change', this._onAccountChange);
        this.listenTo(AccountStore, 'rememberMe', this._onRememberMe);
        this.listenTo(AccountStore, 'logout', this._onLogout);
        this.listenTo(AccountStore, 'loaded', this._accountLoaded);
        this.listenTo(ConfigStore, 'change', this._onConfigChange);

    },
    checkLoginToken: function () { //@override me
        if (Project.debug) {
            console.warn('App does not override check login token');
        }
    },
    createLoginToken: function (token, loginType) {  //@override me
        if (Project.debug) {
            console.warn('App does not override create login token');
        }
    },
    loginWithToken: function (type, token) {
        AppActions.loginWithToken(true, type, token);
    },
    _onRememberMe: function () { // account store signified app that it should remember the user
        if (AccountStore.shouldRememberMe()) {
            this.createLoginToken(AccountStore.getToken(), AccountStore.getLoginType());
        }
    },
    _onAccountChange: function () {
        this.setState({
            account: AccountStore.model,
            config: ConfigStore.model,
            ready: ConfigStore.hasLoaded,
        });
    },
    _accountLoaded: function() {
     this._onLogin()
    },
    _onConfigChange: function () {
        this.setState({
            config: ConfigStore.model
        });
        if (ConfigStore.hasLoaded) {
            this.checkLoginToken();
        }
    },
    _onLogin: function () {
        if (Project.debug) {
            console.warn('App does not override on logout');
        }
    },
    _onLogout: function () {
        if (Project.debug) {
            console.warn('App does not override on logout');
        }
    }
};
/**
 * Created by kyle-ssg on 14/02/15.
 */
