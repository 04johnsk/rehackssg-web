/**
 * Created by kylejohnson on 25/05/15.
 */
var ProductStore = require('../stores/product-store'),
    ButtonStore = require('../stores/buttons-store');
module.exports = {
    getProductState: function() {
        return {
            productsReady: ProductStore.hasLoaded,
            products: ProductStore.getProducts(),
            search: ''
        }
    },
    getButtonState: function() {
        return {
            buttonsReady: ButtonStore.hasLoaded,
            buttons: ButtonStore.getButtons(),
            pairings: ButtonStore.getPairings(),
            search: ''
        }
    },
    _buttonsChanged: function() {
        this.setState(this.getButtonState());
    },
    _productsChanged: function() {
        this.setState(this.getProductState());
    },
    componentWillMount: function() {
        this.setState(_.merge({}, this.getButtonState(), this.getProductState()));
        AppActions.getProducts();
        this.listenTo(ProductStore,'change', this._productsChanged);
        this.listenTo(ButtonStore,'change', this._buttonsChanged);
    },
    filter: function(search) {
        search = Utils.safeParseEventValue(search)
        var products = ProductStore.getProducts();
        if (search && products) {
            search = search.toLowerCase();
            products = _.filter(products, function(product) {
                return product.name && product.name.toLowerCase().search(search) >= 0
            })
        }
        this.setState({
            products: products
        });
    }
};