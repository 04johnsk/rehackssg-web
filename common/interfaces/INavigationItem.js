var AppActions = require('../actions/app-actions'),
    ILoginItem = require('./ILoginItem'),
    AccountStore = require('../stores/account-store'),
    ConfigStore = require('../stores/config-store');
module.exports = _.merge({}, ILoginItem, {
    getAccountState: function() {
        return {
            user: AccountStore.getUser()
        }
    },
    getConfigState: function() {
        return {

        }
    },
    getInitialState: function() {
        return _.merge({}, ILoginItem.getInitialState(), this.getAccountState(), this.getConfigState());
    },
    _onAccountChange: function() {
        if (this._lifeCycleState != "UNMOUNTED") {
            this.setState(this.getAccountState());
        }
    },
    componentWillMount: function() {
        this.listenTo(AccountStore, 'change', this._onAccountChange);
    }
});