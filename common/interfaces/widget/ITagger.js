/**
 * Created by kylejohnson on 02/04/15.
 */
module.exports = {
    guid: Utils.GUID(),
    delimiterRegexp: null,

    getInitialState: function () {
        return {
            items: []
        }
    },

    componentWillMount: function() {
        this.delimiterRegexp = new RegExp('[' + this.props.delimiters.join('') + ']+', 'g');
    },

    isDelimiter: function (char) {
        return _.contains(this.props.delimiters, char);
    },

    remove: function (index) {
        var result = this.state.items;
        if (result.length && result[index]) {
            result.splice(index, 1);
            this.setState({
                items: result
            })
        }
        this.changed(result);
    },

    add: function (text) {
        var delimitedText = text,
            result = [];
        if (this.props.delimiters && delimitedText) {
            delimitedText = delimitedText.trim().replace(this.delimiterRegexp, this.guid);
            result = this.state.items.concat(delimitedText.split(this.guid));
            if (result.length) {
                this.setState({
                    items: result
                })
            }
            this.changed(result);
        }

    },

    changed: function (result) {
        if (this.props.onChange) {
            this.props.onChange(_.filter(result, this.isValid),result)
        }
    },

    _backspacePressed: function () {
        if (this.state.items.length) {
            this.remove(this.state.items.length - 1)
        }
    },

    isValid: function (result) {
        return this.props.validate ? this.props.validate(result) : true;
    }
};