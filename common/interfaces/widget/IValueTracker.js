module.exports = {
    propTypes: {
        cooldown: OptionalNumber
    },
    getInitialState: function() {
        return {change:0};
    },
    componentWillReceiveProps: function (nextProps) {
        if (this.props && !isNaN(this.props.value)  && !isNaN(nextProps.value) && this.props.value != nextProps.value) {

            if ((nextProps.id || this.props.id) && nextProps.id!= this.props.id) {
                return;
            }

            this.setState({
                change : nextProps.value - this.props.value
            });
            if (this.props.cooldown) {
                _.debounce(this.reset, this.props.cooldown)()
            }
        }
    },
    reset:function() {
        this.setState({
            change: 0
        })
    }
};