/**
 * Created by kylejohnson on 25/05/15.
 */
var BasketStore = require('../stores/basket-store'),
    ProductStore = require('../stores/product-store');
module.exports = {
    getBasketState: function() {
        return {
            basketReady: BasketStore.hasLoaded && ProductStore.hasLoaded,
            basket: BasketStore.getBasket(),
            basketEmpty: BasketStore.isEmpty(),
            products: ProductStore.getProducts()
        }
    },
    _basketChanged: function() {
        this.setState(this.getBasketState());
    },
    componentWillMount: function() {
        this.setState(this.getBasketState());
        AppActions.getBasket();
        AppActions.getProducts();

        this.listenTo(BasketStore,'change', this._basketChanged);
        this.listenTo(ProductStore,'change', this._basketChanged);
    }
};