var Dispatcher = require('flux').Dispatcher;

module.exports = _.assign(new Dispatcher(), {
    handleViewAction: function(action) {
        var that = this;

        log(action.actionType,  action);
        var payload = {
            source: 'VIEW_ACTION',
            action: action
        };

        _.defer(function(){
            that.dispatch(payload);
        })
    }

});
