var moment = require('moment');
module.exports = {
    enumeration: {
        get: function (value) {
            if (!value) {
                return "";
            }
            return Format.camelCase(value.replace('_', " "));
        },
        set: function (value) {
            return value.replace(' ', "_").toUpperCase();
        }
    },
    date: {
        yearsOld: function (value) {
            if (value) {
                var a = moment(),
                    b = moment(value);
                return a.diff(b, 'years');
            }
        },
        countdown: function (value) {
            var duration;
            if (value) {
                duration = moment.duration({to: moment(value), from: moment()});
                return this.nearestTen(parseInt(duration.asDays())) + 'd ' +
                    this.nearestTen(duration.hours()) + 'h ' + this.nearestTen(duration.minutes()) + 'm';
            }
        },
        ago: function (value) {
            if (value) {
                var m = moment(value).max();
                return m.fromNow();
            }
        },
        dateToInt: function (value) {
            return value ? moment(value).valueOf() : -1
        },
        dateAndTime: function (value) {
            if (value) {
                var m = moment(value);
                return m.isValid() ? m.format("DD/MM/YYYY HH:mm") : "Invalid date"
            }
        },
        format: function (value, format) {
            if (value) {
                var m = moment(value);
                return m.format(format);
            }
        }
    },
    name: {
        full: function (person) {
            if (!person) {
                return '';
            }
            var fn = person.firstName || '',
                sn = person.lastName || '';

            return fn ? fn + ' ' + sn : sn;
        },

        initialAndLast: function (person) {
            var value = this.fullName(person),
                words;

            if (!value) {
                return;
            }
            words = value.split(' ');

            if (words.length > 1) {
                return words[0].charAt(0) + '.' + ' ' + words[words.length - 1];
            }

            return value;

        }
    },
    number: {
        nearestTen: function (value) {
            return value > 10 ? value : '0' + value;
        },
        ordinal: function (value) {
            var s = ["th", "st", "nd", "rd"],
                v = value % 100;
            return value ? value + (s[(v - 20) % 10] || s[v] || s[0]) : '';
        }
    },
    toYesNo: function (bool) {
        return bool ? 'yes' : 'no';
    },
    camelCase: function (val) {
        return val.charAt(0).toUpperCase() + val.slice(1).toLowerCase();
    },
    textAreaToHtml: function (value) {
        return value ? value.replace(/\r?\n/g, '<br />') : null;
    },
    cssImage: function (value) {
        return value ? 'url("' + value + '")' : 'none';
    },
    firstCharsInArray: function (array) {
        var val = '';
        _.each(array, function (item) {
            val += item.charAt(0);
        });
        return val;
    },
    createOrEdit: function (obj) {
        return obj && obj.id ? 'Edit' : 'Create';
    },
    money: function (pence) {
        return (pence / 100).toFixed(2)
    }


};