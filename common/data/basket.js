var data = require('./_data'),
    fetch = require('../lib/fetch');

    module.exports = {
    get: function () {
        return data.get(Project.api.products + 'basket');
    }
};