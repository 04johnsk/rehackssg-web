var data = require('./_data');

module.exports = {
    set: function (data) {
        return data.put(Project.api.live + 'account');
    },
    get: function () {
        return data.get(Project.api.live + 'account');
    },
    login: function (type, code) {
        return data.get(Project.api.live + 'login/'  + type + '/' + code );
    },
    loginWithEmail: function (details) {
        return data.post(Project.api.live + 'login', details);
    },
    register: function (details) {
        return data.post(Project.api.live + 'register', details);
    },
    resetPassword: function (details) {
        return data.post(Project.api.live + 'resetpassword', details);
    }
};