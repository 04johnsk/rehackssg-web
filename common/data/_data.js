
module.exports = {
    token: '',
    type: '',

    status: function (response) {
        if (response.status >= 200 && response.status < 300) {
            return Promise.resolve(response)
        } else {
            return Promise.reject(response)
        }
    },

    objectToFormData: function(object) {
         var data = [];
         _.each(object, function(value, key){
            data[data.length] = encodeURIComponent( key ) + "=" + encodeURIComponent( value );
         })
        return  data.join( "&" ).replace( /%20/g, "+" );
    },

    get: function (url, data) {
        return this._request('get', url, data || null);
    },

    put: function (url, data) {
        return this._request('put', url, data);
    },

    post: function (url, data) {
        return this._request('post', url, data);
    },

    delete: function (url, data) {
        return this._request('delete', url, data);
    },

    _request: function (method, url, data) {
        var options = {
                timeout: 60000,
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                }
            },
            req;

        if (this.token) {
            options.headers['X-Auth-Token'] = this.token;
            options.headers['X-Auth-Type'] = this.type;
        }

        if (data) {
            options.body = JSON.stringify(data);
        }

        req = fetch(url, options);


        return req
            .then(this.status)
            .then(function (response) {
                return response.json()
            });


    },

    setToken: function (_token, type) {
        this.token = _token;
        this.type = type;
    }
};