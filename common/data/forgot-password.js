var data = require('./_data');

module.exports = {
    set: function (emailAddress) {
        return data.post(Project.api.live + 'forgotpassword', {email: emailAddress});
    }
};