/**
 * Created by kyle-ssg on 16/03/15.
 */
var pusher = null;

if (Project.debug) {
    Pusher.log = log
}


var BaseStore = require('../stores/_store'),
    _data = require('./_data'),

    controller = _.merge({}, BaseStore, {
        id: 'socket',
        clientId: null,
        userId: null,
        connected: false,
        connecting: false,
        channel: null,
        socket: null,
        queue: [],
        subscriptions: {},
        'onopen': function () {
            this.connected = true;
            this.connecting = false;
            this.trigger('connected');
            _.each(_.pluck(this.subscriptions, 'subscription'), this.postSubscription);
        },

        'onmessage': function (res) {

                try {
                    res.content = JSON.parse(res.content);
                } catch (e) {

                }

            this.trigger(res.subscriptionId || 'test_nfc', res);

        },

        'onerror': function (res) {
            this.connecting = false;
            this.error = true;
            this.reconnect();
            this.trigger('error');

        },

        'onclose': function () {
            this.connected = false;
            this.connecting = false;
            this.trigger('disconnected');
        },

        reconnect: function () {
            setTimeout(function () {
                this.connect(this.userId);
            }.bind(this), 500);
        },

        disconnect: function () {
            this.socket.close();
        },

        connect: function (userId) { // attempt connection, if it fails simply go through the reconnect process
            if (this.connecting || this.connected) {
                return;
            }

            this.connecting = true;
            this.userId = userId;
            this.clientId = Utils.GUID();

            pusher = new Pusher(Project.pusher.key);
            pusher.connection.bind('connected', this.onopen);
            pusher.connection.bind('failed', this.onerror);
        },

        subscription: function (subscriptionId, subscriptionType, addedData) {
            addedData = addedData || {};

            return _.merge({}, {
                subscriptionId: subscriptionId,
                subscriptionType: '',
                eventType: subscriptionType
            }, addedData)
        },

        subscribe: function (subscriptions, store) {

            _.each(subscriptions, function (subscription) {
                var existingSubscription = this.subscriptions[subscription.subscriptionId + (subscription.subscriptionType || '')];

                //if no subscription is found for this id, create one with this id
                if (!existingSubscription) {
                    subscription.channel = this.postSubscription(subscription);
                    this.subscriptions[subscription.subscriptionId + (subscription.subscriptionType || '')] = {
                        subscription: subscription,
                        stores: {store: true}
                    };
                } else {
                    existingSubscription.stores.store = true;
                }
            }.bind(this));

        },

        postSubscription: function (subscription) {
            var channel = null;
            if (this.connected) {
                channel = pusher.subscribe(subscription.subscriptionType + subscription.subscriptionId);
                channel.bind('pusher:subscription_succeeded', _.partial(this.trigger, 'subscribe-'
                    + subscription.subscriptionId, subscription.subscriptionId));
                channel.bind(subscription.eventType, this.onmessage, this);
                return subscription;
            }
        },

        postUnsubscription: function (subscription) {
            try {
                pusher.unsubscribe(subscription.subscriptionType + subscription.subscriptionId);
            } catch(e){}
            return subscription;
        },

        unsubscribe: function (subscriptions, store) {

            if (this.connected) {

                _.each(subscriptions, function (subscription) {
                    var existingSubscription = this.subscriptions[subscription.subscriptionId + (subscription.subscriptionType || '')];

                    //if no subscription is found for this id, unsubscribe
                    if (!existingSubscription) {
                        this.postUnsubscription(subscription);
                    } else {
                        delete existingSubscription.stores[store];
                        if (!existingSubscription.stores.length) {
                            this.postUnsubscription(subscription);
                            delete this.subscriptions[subscription.subscriptionId + (subscription.subscriptionType || '')];
                        }
                    }
                }.bind(this));

            }
        },

        get: function (url, data) {
            return _data.get(Project.api.channels + url, data).catch(function () {
                setTimeout(function () {
                    this.get(url, data)
                }.bind(this), 200);
            }.bind(this));
        },

        post: function (url, data) {
            return _data.post(Project.api.channels + url, data).catch(function () {
                setTimeout(function () {
                    this.post(url, data)
                }.bind(this), 200);
            }.bind(this));
        }
    });

_.bindAll(controller, 'onopen', 'postSubscription', 'onclose', 'onmessage', 'onerror', 'subscribe', 'unsubscribe', 'trigger');

module.exports = controller;