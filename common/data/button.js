var data = require('./_data'),
    accessToken = "2c09b1de26ebfed9f49b281e383da97b2c4ed598",
    fetch = require('../lib/fetch');
require('../lib/event-source');

module.exports = {
    get: function () {
        return data.get(Project.api.apiary + 'buttons');
    },
    getAttr: function (button, variable) {
        return fetch(Project.api.spark + 'devices/' + button + '/' + variable + '?access_token=' + accessToken, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).then(function (response) {
            return response.json()
        });
    },
    subscribe: function (button, event) {
        return new EventSource(Project.api.spark + 'devices/' + button + '/events/' + event + '?access_token=' + accessToken)
    },
    click: function (button) {
        return fetch(Project.spark + 'devices/' + button + '/digitalwrite', {
            method: 'post',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: data.objectToFormData({
                access_token: accessToken,
                params: 'D0,HIGH'
            })
        }, true);
    }
};