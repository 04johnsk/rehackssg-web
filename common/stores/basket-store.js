var BaseStore = require('./_store'),
    api = require('../data/basket');


var controller = {
        get: function () {
            if (!store.model) {
                store.loading();
                api.get()
                    .then(this.loaded)
                    .catch(_.partial(AjaxHandler.error, store));
            } else {
                store.loaded();
            }
        },

        loaded: function (data) {
            store.model = data;
            store.loaded();
        }
    },
    store = _.assign({}, BaseStore, {
        id: 'basket',
        getBasket: function() {
          return this.model? this.model : null;
        },
        isEmpty: function() {
            return this.model ? _.size(this.model)<=0 : null;
        },
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.GET_BASKET:
                    controller.get();
                    break;
                case Actions.SET_QUANTITY:
                    if (action.quantity>0) {
                        store.model[action.productId] = action.quantity;
                    } else {
                        delete store.model[action.productId];
                    }
                    store.changed();
                    break;
                default:
                    return;
            }
        })
    });

controller.store = store;
module.exports = controller.store;