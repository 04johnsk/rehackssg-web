var BaseStore = require('./_store'),
    api = require('../data/button');


var controller = {
        click: function (button) {
            store.loading[button] = true;
            api.click(button)
                .then(_.partial(this.loaded, button))
                .catch(_.partial(AjaxHandler.error, store));
            store.changed();
        },

        loaded: function (button, data) {
            store.model = data;
            store.loading[button] = false;
            store.changed();
        }
    },
    store = _.assign({}, BaseStore, {
        id: 'button',
        loading: {},
        getLoading: function () {
            return this.loading;
        },
        getButtons: function () {
            return this.model ? this.model.buttons : null;
        },
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.ClICK_BUTTON:
                    controller.click(action.id);
                    break;
                default:
                    return;
            }
        })
    });

controller.store = store;
module.exports = controller.store;