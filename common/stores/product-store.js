var BaseStore = require('./_store'),
    sockets = require('../data/_socket'),
    api = require('../data/products');


var controller = {
        subscriptions: [],
        get: function () {
            if (!store.model) {
                store.loading();
                api.get()
                    .then(this.loaded)
                    .catch(_.partial(AjaxHandler.error, store));
            } else {
                store.loaded();
            }
        },

        addProduct: function() {
            // See https://adimo.co/api/documentation
            endpoint = "https://api.adimo.co/v2";
            campaignId = 495;
            brandId = 46;

            supermarketAmazonUk   = 10;
            supermarketAsda       =  2;
            supermarketBoots      =  9;
            supermarketOcado      =  3;
            supermarketSainsburys =  4;
            supermarketSuperdrug  = 15;
            supermarketTesco      =  7;
            supermarketWaitrose   =  5;

            developerKey   = "WTHDLvg4MapCRWfb8PyM";
            applicationKey = "9E642BA54948E1E29D5E";

            sainsburysEmail     = "howard@bluesoup.co.uk";
            sainsburysPassword  = "password";

            $(document).ready(function() {
                    $(".hidden").hide();

                    console.log("Logging in");

                    $.ajax({
                        url: endpoint + "/Login",
                        type: "POST",
                        data: {
                            "CampaignId": campaignId,
                            "BrandId": brandId,
                            "SupermarketId": supermarketSainsburys,

                            "Email": sainsburysEmail,
                            "Password": sainsburysPassword
                        }
                    }).done(function(response) {
                        sessionKey = response.RecipopSessionKey;

                        console.log(response);
                        $.ajax({
                            url: endpoint + "/SearchProducts",
                            type: "POST",
                            data: {
                                "CampaignId": campaignId,
                                "BrandId": brandId,
                                "SupermarketId": supermarketSainsburys,

                                "SessionKey": sessionKey,
                                "SearchTerm": "Cif Bathroom Spray 700ml"
                            }
                        }).done(function(response) {
                            productId = response.Products[0].ProductId;

                            console.log(response);
                            $.ajax({
                                url: endpoint + "/AddProduct",
                                type: "POST",
                                data: {
                                    "CampaignId": campaignId,
                                    "BrandId": brandId,
                                    "SupermarketId": supermarketSainsburys,

                                    "SessionKey": sessionKey,
//                          "ProductID": "6355645", // Cif @ Sainsbury's
                                    "ProductID": productId,
                                    "Quantity": 1
                                }
                            }).done(function(response) {
                                console.log(response);
                                $("#addToBasket").show();
                                $.ajax({
                                    url: endpoint + "/GetBasket",
                                    type: "GET",
                                    data: {
                                        "CampaignId": campaignId,
                                        "BrandId": brandId,
                                        "SupermarketId": supermarketSainsburys,

                                        "SessionKey": sessionKey
                                    }
                                }).done(function(response) {
                                    store.changed();
                                });
                            });
                        });
                    });

                    console.log("Search for a product");


                    console.log("Add product to basket");



                    console.log("Retrieve basket");


                }
            );
        },

        loaded: function (data) {
            store.model = _.indexBy(data, 'id');
            store.loaded();
        },
        _onNfc: function() {
            store.trigger('nfc');
        }
    },
    store = _.assign({}, BaseStore, {
        id: 'products',
        getProducts: function() {
          return this.model? this.model : null;
        },
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.ADD_PRODUCT:
                    controller.addProduct();
                    break;
                default:
                    return;
            }
        })
    });
sockets.connect('test');
controller.subscriptions.push(sockets.subscription('channel-one', 'test_nfc'));
sockets.subscribe(controller.subscriptions);
sockets.on('test_nfc', controller._onNfc);
controller.store = store;
module.exports = controller.store;