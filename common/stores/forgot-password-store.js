var BaseStore = require('./_store'),
    api = require('../data/forgot-password');


var controller = {
        process: function (emailAddress) {
            store.saving();
            api.set(emailAddress)
                .then(this.saved)
                .catch(_.partial(AjaxHandler.error, store));
        },

        saved: function (data) {
            store.model = data;
            store.saved();
        }
    },
    store = _.assign({}, BaseStore, {
        id: 'forgotpassword',
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.FORGOT_PASSWORD:
                    controller.process(action.emailAddress);
                    break;
                default:
                    return;
            }
        })
    });

controller.store = store;
module.exports = controller.store;