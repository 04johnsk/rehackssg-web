var BaseStore = require('./_store'),
    _data = require('../data/_data'),
    _accountAPI = require('../data/account'),
    store = _.assign({}, BaseStore, {
        id: 'account',
        model: null,
        rememberMe: true,
        token: null,
        loginType: null,
        shouldRememberMe: function () {
            return this.rememberMe;
        },
        getInvites: function () {
            return this.model ? this.model.invites : null;
        },
        loginDetailsValid: function (details) {
            return Utils.validate.email(details.emailAddress) && details.password;
        },
        registrationDetailsValid: function (details) {
            return details.firstName && details.lastName && Utils.validate.email(details.emailAddress1) && (details.emailAddress1 == details.emailAddress2) && details.password1 && (details.password1 == details.password2);
        },
        resetDetailsValid: function (details) {
            return Utils.validate.email(details.emailAddress) && details.password1 && (details.password1 == details.password2);
        },
        getUser: function () {
            return this.model ? this.model.user : null;
        },
        isValidUser: function (user) {
            return user.firstName && user.lastName;
        },
        getToken: function () {
            return this.token;
        },
        getLoginType: function () {
            return this.loginType;
        },
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.LOGIN_WITH_TOKEN:
                    store.rememberMe = action.rememberMe;
                    store.loginType = action.type;
                    _handleTokenLogin(action.token);
                    break;
                case Actions.LOGIN:
                    store.rememberMe = action.rememberMe;
                    store.loginType = action.type;
                    _handleLogin(action.token);
                    break;
                case Actions.LOGIN_WITH_EMAIL:
                    store.loginType = 'EMAIL';
                    _loginWithEmail(action.loginDetails);
                    break;
                case Actions.LOGOUT:
                    _logout();
                    break;
                case Actions.REGISTER:
                    store.loginType = 'EMAIL';
                    _register(action.registration);
                    store.changed();
                    break;
                case Actions.RESET_PASSWORD:
                    store.loginType = 'EMAIL';
                    _resetPassword(action.resetDetails);
                    store.changed();
                    break;
                case Actions.SET_ACCOUNT:
                    store.saving();
                    _accountAPI.set(action.user)
                        .then(_userSaved)
                        .catch(_.partial(AjaxHandler.error, store));
                    break;
                default:
                    return;
            }
        })
    });

function _userSaved(data) {
    store.model.user = data;
    store.saved()
}

function _register(details) {
    store.loading();
    _accountAPI.register(details)
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _logout() {
    store.model = null;
    _data.setToken('');
    store.token = null;
    store.changed();
    store.trigger('logout');
}

function _refresh() {
    _accountAPI.get()
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _handleLogin(token) {
    store.loading();
    _accountAPI.login(store.loginType, token)
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _resetPassword(details) {
    store.loading();
    _accountAPI.resetPassword(details)
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _loginWithEmail(details) {
    store.loading();
    _accountAPI.loginWithEmail(details)
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _handleTokenLogin(token) {
    store.loading();
    _data.setToken(token, store.loginType);
    store.token = token;

    _accountAPI.get()
        .then(_accountLoaded)
        .catch(_.partial(AjaxHandler.error, store));
}

function _accountLoaded(data) {
    var previousModel = store.model ? true : false;
    store.model = data;

    if (data.authToken) {
        _data.setToken(data.authToken, store.loginType);
        store.token = data.authToken;
    }

    store.loaded();
    if (store.rememberMe) {
        store.trigger('rememberMe');
    }
}


module.exports = store;