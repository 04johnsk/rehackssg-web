var BaseStore = require('./_store'),
    api = require('../data/config');


var controller = {
        get: function () {
            store.loading();
            api.get()
                .then(this.loaded)
                .catch(_.partial(AjaxHandler.error, store));
        },
        loaded: function (data) {
            store.model = data;
            store.model.liveClubs = _.indexBy(store.model.liveClubs, 'id');
            store.loaded();
        }
    },
    store = _.assign({}, BaseStore, {
        id: 'config',
        model: null
    });

controller.get();
controller.store = store;
module.exports = controller.store;