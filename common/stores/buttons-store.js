var BaseStore = require('./_store'),
    api = require('../data/button');


var controller = {
        get: function () {
            if (!store.model) {
                store.loading();
                api.get()
                    .then(this.loaded)
                    .catch(_.partial(AjaxHandler.error, store));
            } else {
                store.loaded();
            }
        },

        loaded: function (data) {
            store.model = {};
            store.pairings = {};
            if (data && data.buttons) {
                _.each(data.buttons, controller.subscribeButton);
            }
            store.loaded();
        },
        updateButton: function (data) {
            var buttonId = data.coreInfo.deviceID,
                variable = data.name;

            store.model[buttonId][variable] = data.result;
            store.changed();
            return data;
        },
        setPairing: function (data) {
            var buttonId = data.coreInfo.deviceID;
            store.pairings[data.result] = buttonId;
        },
        subscribeButton: function (buttonId) {
            store.model[buttonId] = {
                ready: false
            };
            api.getAttr(buttonId, 'productId')
                .then(controller.updateButton)
                .then(controller.setPairing)
                .catch(function () {
                    log('Error getting productId for ' + buttonId);
                });

            api.getAttr(buttonId, 'quantity')
                .then(controller.updateButton)
                .catch(function () {
                    log('Error getting quantity for ' + buttonId);
                });

            var eventSource = api.subscribe(buttonId, 'productId')
                .onmessage = controller.onMessage;

            //this would normally happen on the server and get pushed to the client
            api.subscribe(buttonId, 'quantity')
                .addEventListener("quantity", function (e) {
                    var result = JSON.parse(e.data),
                        id = result.coreid,
                        button = store.model[id],
                        value = parseInt(result.data);
                    if (button) {
                        AppActions.setProductQuantity(button.productId, value);
                    }
                })
        }

    },
    store = _.assign({}, BaseStore, {
        id: 'button',
        model: null,
        pairings: null,
        getButtons: function () {
            return this.model ? this.model.buttons : null;
        },
        getPairings: function () {
            return this.pairings;
        },
        getPairing: function (id) {
            return this.pairings ? this.pairings[id] : null
        },
        dispatcherIndex: Dispatcher.register(function (payload) {
            var action = payload.action; // this is our action from handleViewAction

            switch (action.actionType) {
                case Actions.GET_BUTTONS:
                    controller.get();
                    break;
                default:
                    return;
            }
        })
    });

controller.store = store;
module.exports = controller.store;