/** @jsx React.DOM */

module.exports = function (options) {
    return React.createClass( _.assign({}, options, {
        _listeners: [],

        listenTo: function (store, event, callback) {
            this._listeners.push({
                store: store,
                event: event,
                callback: callback
            });
            store.on(event, callback);
            return this._listeners.length;
        },

        stopListening: function(index) {
            var listener = this._listeners[index];
            listener.store.off(listener.event, listener.callback);
        },

        req: function(val) {
            return val? 'validate valid' : 'validate invalid' ;
        },

        componentWillUnmount: function () {
            _.each(this._listeners, function (listener, index) {
                this.stopListening(index);
            }.bind(this));
            return options.componentWillUnmount ? options.componentWillUnmount() : true;
        }
    }));
};
