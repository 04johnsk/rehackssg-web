import cgi
import urllib

import jinja2
import webapp2
import os
import logging

from google.appengine.api import users
from google.appengine.ext import ndb
from google.appengine.ext.webapp import template
from google.appengine.api import memcache


JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)

class Author(ndb.Model):
    identity = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)

# ---- main handler
class MainRedirect(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        logging.info('In Mainredirect')
        logging.info('path: ' + self.request.path)
        self.redirect('/app?path=' + urllib.quote(self.request.path))

class MainPage(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        logging.info('In MainPage')
        logging.info('path: ' + self.request.path)
        self.response.headers['Content-Type'] = 'text/html'
        path = os.path.join(os.path.dirname(__file__), 'dist/app.html')
        f = open(path, 'r')
        self.response.out.write(f.read())

class TemplatePage(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        logging.info('In templatepage')
        logging.info('path: ' + self.request.path)
        self.response.headers['Content-Type'] = 'text/html'
        path = os.path.join(os.path.dirname(__file__), 'dist' + self.request.path)
        f = open(path, 'r')
        self.response.out.write(f.read())

class HomePage(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        template = JINJA_ENVIRONMENT.get_template('dist/index.html')

class ContactUsPage(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        serveStaticPage(self, 'dist/contact-us.html')

class TermsAndConditionsPage(webapp2.RequestHandler):
    def get(self, *args, **kwargs):
        serveStaticPage(self, 'dist/tsandcs.html')

def serveStaticPage(serve, template_location):
    template = JINJA_ENVIRONMENT.get_template(template_location)
    serve.response.write(template.render())

application = webapp2.WSGIApplication([
    (r'/login', MainPage),
    (r'/app', MainPage),
    (r'/templates/.*', TemplatePage),
    (r'/', MainPage),
    (r'/contact-us', ContactUsPage),
    (r'/tsandcs', TermsAndConditionsPage),
    (r'/.*', MainRedirect),
], debug=True)