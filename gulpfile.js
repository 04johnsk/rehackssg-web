var gulp = require('gulp');
var source = require('vinyl-source-stream'); // Used to stream bundle for further handling
var browserify = require('browserify');
var watchify = require('watchify');
var reactify = require('reactify');
var gulpif = require('gulp-if');
var streamify = require('gulp-streamify');
var notify = require('gulp-notify');
var replace = require('gulp-replace');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var gutil = require('gulp-util');
var livereload = require('gulp-livereload');
var rev = require('gulp-rev');
var del = require('del');
var filter = require('gulp-filter');

// External dependencies you do not want to rebundle while developing,
// but include in your application deployment
var dependencies = [];

var browserifyTask = function (options) {

    // Our app bundler
    var appBundler = browserify({
        entries: [options.src], // Only need initial file, browserify finds the rest
        transform: [reactify], // We want to convert JSX to normal javascript
        debug: options.development, // Gives us sourcemapping
        cache: {}, packageCache: {}, fullPaths: options.development // Requirement of watchify
    });

    // todo, eventually we will take all of our libs and bundle them into a vendors.js.
    appBundler.external([]);

    // The rebundle process
    var rebundle = function () {
        var start = Date.now();
        console.log('Building APP bundle');
        appBundler.bundle()
            .on('error', gutil.log)
            .pipe(source('main.js'))
            .pipe(gulp.dest(options.dest))
            .pipe(gulpif(options.development, livereload()))
            .pipe(notify(function () {
                console.log('APP bundle built in ' + (Date.now() - start) + 'ms');
                if (!options.development) {
                    cacheBuster();
                }
            }));
    };


    // Fire up Watchify when developing
    if (options.development) {
        appBundler = watchify(appBundler);
        appBundler.on('update', rebundle);
    }

    if (!options.development) {
        del(['./dist/main*.js'], function () {
            rebundle();
        });
    } else {
        rebundle();
    }
};

var cssTask = function (options) {
    var start = new Date();
    if (options.development) {
        var run = function () {
            gulp.src(options.src)
                .pipe(concat('main.css'))
                .pipe(gulp.dest(options.dest))
                .pipe(livereload())
                .pipe(notify(function () {
                    console.log('CSS bundle built in ' + (Date.now() - start) + 'ms');
                }));
        };
        run();
        gulp.watch(options.src, run);
    } else {
        gulp.src(options.src)
            .pipe(concat('main.css'))
            .pipe(cssmin())
            .pipe(gulp.dest(options.dest))
            .pipe(notify(function () {
                console.log('CSS bundle built in ' + (Date.now() - start) + 'ms');
            }));
    }
};

var assetTask = function (options) {
    gulp.src(options.src)
        .pipe(gulp.dest(options.dest))
};

var cacheBuster = function () {
    return gulp.src(['./dist/cache/main.css', './dist/cache/main.js'])
        .pipe(rev())
        .pipe(gulp.dest('./dist/cache'))
        .pipe(rev.manifest())
        .pipe(gulp.dest('./dist/cache'))
        .pipe(notify(function () {
            revDel()
        }));
};

var revDel = function () {
    var manifest = require('./dist/cache/rev-manifest.json');
    del('./dist/cache/main.js');
    del('./dist/cache/main.css');

    gulp.src(['./dist/*.html'])
        .pipe(replace(/main.js/g, 'cache/' + manifest['main.js']))
        .pipe(replace(/main.css/g, 'cache/' + manifest['main.css']))
        .pipe(replace(/<script.*?livereload.js.*?\/script>/g, ''))
        .pipe(gulp.dest('./dist'));



};

// Starts our development workflow
gulp.task('default', function () {

    browserifyTask({
        development: true,
        src: './src/js/main.js',
        dest: './dist'
    });

    cssTask({
        development: true,
        src: './src/styles/**/*.css',
        dest: './dist'
    });

    assetTask({
        development: true,
        src: './src/**.html',
        dest: './dist'
    });

    assetTask({
        development: true,
        src: './src/images/**',
        dest: './dist/images'
    });

    assetTask({
        development: true,
        src: './src/signup/**',
        dest: './dist/signup/'
    });

    assetTask({
        development: true,
        src: './src/fonts/**',
        dest: './dist/fonts/'
    });


});

gulp.task('deploy', function () {


    del(['./dist/**'], function () {
        browserifyTask({
            development: false,
            src: './src/js/main.js',
            dest: './dist/cache'
        });

        cssTask({
            development: false,
            src: './src/styles/**/*.css',
            dest: './dist/cache'
        });

        assetTask({
            development: false,
            src: './src/**.html',
            dest: './dist'
        });

        assetTask({
            development: false,
            src: './src/images/**',
            dest: './dist/images'
        });

        assetTask({
            development: false,
            src: './src/fonts/**',
            dest: './dist/fonts/'
        });

    });


});

gulp.task('master', function () {

    del(['./dist/**'], function () {

        cssTask({
            development: false,
            src: './src/styles/**/*.css',
            dest: './dist'
        });

        assetTask({
            development: false,
            src: './src/**.html',
            dest: './dist'
        });

        assetTask({
            development: false,
            src: './src/images/**',
            dest: './dist/images'
        });

        assetTask({
            development: false,
            src: './src/signup/**',
            dest: './dist/signup/'
        });

        assetTask({
            development: false,
            src: './src/fonts/**',
            dest: './dist/fonts/'
        });
    });

});

