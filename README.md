## Install ##

```
#!bash
brew install google-app-engine
npm install gulp -g

```

## Run ##
Window 1 - live reloading of assets

```
#!bash
npm install
gulp
```

Window 2 - running the web server
```
#!bash

./run-local.sh
```

## Production Build (concat, cachebusting etc) ##
gulp deploy

## Configure ##

* **project.js** - contains api urls / keys
* **constants.js** - contains constaints
* **main.js** - contains routes
* **window.js** - require libs and add to global scope

## Flux Stuff (contained in /common) ##

* **app-actions.js** - contains the actions which components may call
* **action-constants.js** - array of enums which correspond to actions
* **/interfaces** - interfaces for components to interact with
* **/stores** - flux stores
* **/data** - thin ajax layer managed with fetch.js